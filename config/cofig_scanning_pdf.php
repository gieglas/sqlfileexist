<?php
$n_options = array(
		"connectionfrom" => array (			
			"server" => "10.10.10.10",		
			"port" => "",
			"user" => "user",
			"pass" => "pass",
			"name" => "dbname", 
			"provider" => "sqlsrv",
			"convertencoding" => false,
			"in_charset" => "",
			"out_charset" => ""
		),	
		"connectionto" => array (			
			"server" => "10.10.10.10",		
			"port" => "",
			"user" => "user",
			"pass" => "pass",
			"name" => "dbname", 
			"provider" => "sqlsrv",
			"convertencoding" => false,
			"in_charset" => "",
			"out_charset" => ""
		),			
		"selectqueryfrom" => "select idapplication as [id], 
			docpath as [path],
			idapplication + ' - ' + convert(nvarchar(100),doctype) + ' - ' + description as [info] 
			from SCANNINGmarkdocumentdata",
		"printonnotfound" => false,		
		"insertonnotfound" => false,
		"notfoundinsert" => "insert into [TempTMScanning].[dbo].[_SCANNINGErrors$] 
			(checkId, checkName, checkDesc, idapplication, values, checkDate)
			values (1000, 'MarkDocumentData - File does not Exist', 'Checks if files defined in MarkDocumentData exist',:var0,:var1 + ' - ' + :var2, getdate() )",
		"printonfound" => false,
		"insertonfound" => false,
		"foundinsert" => "insert into [TempTMScanning].[dbo].[_SCANNINGErrors$] 			
			values (1001, 'MarkDocumentData - File Exists', 'Checks if files defined in MarkDocumentData exist',:var0,:var1 + ' - ' + :var2, getdate() )",
		"showonlogfile" => true,
		"logfile" => "logs/scanning_pdf_#yyyy#-#mm#-#dd#.log",
		"showonscreen" => true
	);


?>