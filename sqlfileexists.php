<?php
/**
 sqlfileexist - Check if file exists based on a query 
 --------------------
 
  @author      Constantinos Evangelou <gieglas@gmail.com>
  @copyright   2019 Constantinos Evangelou
  @license     MIT License (MIT)
  @version     1.0.0
  @date		   2019/10/04
 
 */
 
 /* example how to run
C:\php\php.exe  -f C:\code\sqlFileExists\sqlFileExists.php config\config.php
*/

if (!_is_cli()) {
	echo "This script can only be run from the command line";
} else {
	if ($argc != 2) {
		echo "This is a command line PHP script with 1 argument. \r\nPlease enter the config file as a first argument.";
	}
	else {
		try {
			//error reporting setting
			error_reporting(E_ALL & ~E_NOTICE);
			//load configuration file
			require_once $argv[1];						
			//local vars
			$Data=array();
			$insertRes=array();
			$parametersArr=array();			
			$recCounter=0;
			$colCounter=0;
			$errorCount=0;
			$foundCount=0;
			$notFoundCount=0;
			_echoMessage(" ----------  sqlfileexist Started ---------- \r\n",true);
			//use global variables from config.php
			global $n_options;	
				
				
			/**************************************************************/
			//1) get the data from the source database	([id],[path],[info])
			/**************************************************************/			
			$Data=_getData($n_options["selectqueryfrom"],$n_options["connectionfrom"],$parametersArr,"Array");
			if ($Data["error"]) {
				$errorCount++;
				_echoMessage("----- ERROR ------ _getData". $Data["error"] . "\r\n");
				exit;
			}		
			_echoMessage("Amount of files to check : " . count($Data) . "\r\n",true);
			/**************************************************************/
			//2) check if file exists for each record 
			/**************************************************************/
			//for each entry in data
			foreach ($Data as $res) {
				$recCounter++; //count records inserted
				$parametersArr=array();
				$colCounter=0;
				
				/* load record data into bind variables */
				foreach ($res as $item) {
					$parametersArr=_addSQLParameter("var$colCounter",$item,$parametersArr);
					$colCounter++;
				}
				
				
				try {
					if (file_exists($res["path"])) {
						//found
						$foundCount ++;
						
						if ($n_options["printonfound"]) {
							_echoMessage("RECORD FOUND:".implode(", ", $res)."\r\n",true);							
						}
						
						if ($n_options["insertonfound"]) {
							$insertRes=_exeCommand($n_options["foundinsert"],$n_options["connectionto"],$parametersArr,"Array");
							if ($insertRes["error"]) {
								$errorCount++;
								_echoMessage("----- ERROR ------ EXCEPTION:".$insertRes["error"]." ON RECORD: ".implode(", ", $res)."\r\n",true);
							}							
						}					
					} else {
						//not found
						$notFoundCount++;
						
						if ($n_options["printonnotfound"]) {
							_echoMessage("RECORD NOT FOUND:".implode(", ", $res)."\r\n",true);
						}
						
						if ($n_options["insertonnotfound"]) {
							$insertRes=_exeCommand($n_options["notfoundinsert"],$n_options["connectionto"],$parametersArr,"Array");
							if ($insertRes["error"]) {
								$errorCount++;
								_echoMessage("----- ERROR ------ EXCEPTION:".$insertRes["error"]." ON RECORD: ".implode(", ", $res)."\r\n",true);
							}							
						}
					}
				}
				catch (exception $e) {
					$errorCount++;
					_echoMessage("----- ERROR ------ EXCEPTION:".$e->getMessage()." ON RECORD: ".implode(", ", $res)."\r\n",true);
				}
				
				if (($n_options["maxerrors"] != -1) && ($errorCount > $n_options["maxerrors"])) {
					_echoMessage("----- ERROR ------ Maximum number of errors exceeded, aborting... \r\n",true);
					break 2; //exit both loops
				}
			}

		} catch(exception $e) { 
			_echoMessage("----- ERROR ------ \r\n",true);
		}
		_echoMessage("----------  Process Finished. \r\n",true);
		_echoMessage("Processed : $recCounter \r\n",true);
		_echoMessage("Found: $foundCount \r\n",true);
		_echoMessage("Not Found: $notFoundCount \r\n",true);
		_echoMessage("Errors : $errorCount \r\n",true);
		_echoMessage("----------  sqlfileexist Finished ---------- \r\n",true);
	}
}

//---------------------------------------------------------------
//-------PRIVATE
function _getData($query,$connection,$parameters = array(),$format="JSON") {
	//run code depending on provider ... e.g. handle differently mySQL with SQL Server and execute
	switch ($connection["provider"])
	{		
	case "mysql":
	case "sqlsrv":
	case "oci":
	case "SYBASE" :
	//----------------------- PDO SERVERS ------------------------
		try {
			//get connection
			$conn = _getOpenPDOConnection($connection);
			//execute the SQL statement and return records
			$st = $conn->prepare($query);

			//add parameters
			foreach($parameters as $parameterData) {
				if ($connection["convertencoding"] && !empty($parameterData->value)) {
					$parameterData->value = iconv($connection["in_charset"], $connection["out_charset"]."//IGNORE", $parameterData->value);
				}
				$st->bindParam(':'.$parameterData->name,$parameterData->value);						
			}
			
			$st->execute();
			$rs=$st->fetchAll(PDO::FETCH_ASSOC);
			
			//depending on the format return the appropriate type
			if ($format=="JSON") {
				return json_encode($rs);	
			}
			else {
				return $rs;
			}
			
			$rs = null;
			$conn = null;
		} catch(exception $e) { 
			if ($format=="JSON") {
				return '{"error":"'. $e->getMessage() .'"}'; 
			} else {
				return array(
					"error" => $e->getMessage()
				); 
			}			
		} 
		break;
	}
}

//---------------------------------------------------------------
//-------PRIVATE
function _exeCommand($query,$connection,$parameters = array(),$format="JSON") {
//run code depending on provider ... e.g. handle differently mySQL with SQL Server and execute
	switch ($connection["provider"])
	{		
	case "mysql":
	case "sqlsrv":
	case "oci":
	case "SYBASE" :
	//----------------------- PDO SERVERS ------------------------
		try {
			//get connection
			$conn = _getOpenPDOConnection($connection);
			//execute the SQL statement and return records
			$st = $conn->prepare($query);

			//add parameters
			foreach($parameters as $parameterData) {				
				//if we need to change characterset do so
				if ($connection["convertencoding"] && !empty($parameterData->value)) {
					$parameterData->value = iconv($connection["in_charset"], $connection["out_charset"]."//IGNORE", $parameterData->value);
				}
				$st->bindParam(':'.$parameterData->name,$parameterData->value);
			}
			
			$st->execute();
			
			//if no error return success
			//depending on the format return the appropriate type
			if ($format=="JSON") {
				return '{"success":"success"}'; 
			}
			else {
				return array(
					"success" => "success"
				);
			}
			
			$rs = null;
			$conn = null;
		} catch(exception $e) { 
			if ($format=="JSON") {
				return '{"error":"'. $e->getMessage() .'"}'; 
			} else {
				return array(
					"error" => $e->getMessage()
				); 
			}			
		} 
		break;
	}
}
//--------------------------------------------------------------
//-------PRIVATE
/*
	add parameter to an array and return array of parameters
*/
function _addSQLParameter($name,$value,$params) {
	$parameterObj=new stdClass();
	$parameterObj->name=$name;
	$parameterObj->value=$value;
	array_push($params, $parameterObj);
	return $params;
}
//--------------------------------------------------------------
//-------PRIVATE
/*
	extension=php_pdo_sqlsrv_53_ts.dll in php.ini and Microsoft SQL Server 2008 R2 Native Client http://craigballinger.com/blog/2011/08/usin-php-5-3-with-mssql-pdo-on-windows/
	FOR oci ORACLE, need instantclient_12_1 and add its path to PATH in SYSTEM Enviromental Variables. Note Oracle supports 2 versions down so select your client version properly
	FOR Sybase the PDO_ODBC is used. In order to work must have Sybase ASE ODBC Driver which comes with the SDK. 
*/
function _getOpenPDOConnection($connection) {
	
	$myProvider = $connection["provider"];
	$myServer = $connection["server"];
	$myUser = $connection["user"];
	$myPass = $connection["pass"];
	$myDB = $connection["name"]; 
	$myPort = $connection["port"]; 
	
	//define connection string, specify database driver
	switch ($myProvider) {
	case "mysql":
		$connStr = "mysql:host=".$myServer.";dbname=".$myDB; 
		$conn = new PDO($connStr,$myUser,$myPass);			
		break;
	case "sqlsrv":
		$connStr = "sqlsrv:Server=".$myServer.";Database=".$myDB; 
		$conn = new PDO($connStr,$myUser,$myPass);	
		break;
	case "oci":
		$tns = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = ".$myServer.")(PORT = ".$myPort.")))(CONNECT_DATA=(SID=".$myDB.")))"; 
		$connStr = "oci:dbname=".$tns.";charset=utf8";
		$conn = new PDO($connStr,$myUser,$myPass);	
		$conn->setAttribute(PDO::ATTR_AUTOCOMMIT,TRUE);		
		break;
	case "SYBASE":
		$connStr = "odbc:Driver={Adaptive Server Enterprise};server=".$myServer.";port=".$myPort.";db=".$myDB;		
		$conn = new PDO($connStr,$myUser,$myPass);	
		break;
	}

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $conn;
}
//-------------------------------------------------------------
//-------PRIVATE
function _echoMessage($msg,$showDatetime=false) {
	//use global variables from config.php
	global $n_options;
	$msg=($showDatetime)?date("Y-m-d H:i:s")."| ".$msg:$msg;
	if ($n_options["showonlogfile"]) {
		file_put_contents(_replaceSpecialTags($n_options["logfile"]), $msg, FILE_APPEND | LOCK_EX);
	}
	if ($n_options["showonscreen"]) {
		echo ($msg);
	}
}
//-------------------------------------------------------------
//-------PRIVATE
function _replaceSpecialTags($strIn) {
	$now = new DateTime();
	$strOut = $strIn;
	$strOut = str_replace("#dd#", $now->format("d"),$strOut);
	$strOut = str_replace("#mm#", $now->format("m"),$strOut);
	$strOut = str_replace("#yyyy#", $now->format("Y"), $strOut);

	return $strOut;
}

//-------------------------------------------------------------
//-------PRIVATE
/*Check if it is run on commandline*/
function _is_cli()
{	
	return php_sapi_name() === 'cli';
}
?>